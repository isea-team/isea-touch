#电容触摸按键

##pcb
![板子正面](isea-touch-front.png "正面视图")
----------------------
![板子背面](isea-touch-back.png "背面视图")
###拼版打样（4 * 3 张，尺寸83*83 mm^2  ），预览图无法显示切割线，实际以gerber文件\*.gm1为切割线定义
----------------------
![板子正面](merged_4x3/isea-touch-merged_front.png "正面视图")
![板子背面](merged_4x3/isea-touch-merged_back.png "背面视图")
